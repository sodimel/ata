from pathlib import Path

from .git import HandleGitRepo


class HandleFilesAndFolders:
    def __init__(self, ns=None):
        self.root_folder = Path.home() / ".ata"
        if ns is not None:
            self.select_namespace(ns)
        else:
            self.current_namespace = None
        self.check_or_create_root_folder()

    def check_or_create_root_folder(self):
        if self.root_folder.exists():
            if not self.root_folder.is_dir():
                print(f"Error: {self.root_folder} is a file and not a folder!")
        else:
            Path.mkdir(self.root_folder)
            print(f"Info: Created folder {self.root_folder}.")

    # UTILS (mkdir, ls, touch)
    def create_folder(self, path):
        path.mkdir()

    def create_file(self, path):
        path.touch()

    def print_folders(self, path, recursive=False):
        count = 1
        if recursive:
            for file in path.rglob("*"):
                if file.is_dir():
                    print(count, end=" − ")
                    count +=1
                    print(str(file).replace(str(Path().home() / ".ata"), "")[1:])
        else:
            for file in path.glob("*"):
                if file.is_dir():
                    print(count, end=" − ")
                    count +=1
                    print(str(file).replace(str(Path().home() / ".ata"), "")[1:])

    def print_file(self, path):
        f = open(f"{path}", "r")
        print(f.read())
        f.close()

    def overwrite_file(self, path, content):
        with open(path, "w") as file:
            file.write(content)

    def rm_tree(self, pth: Path):
        for child in pth.iterdir():
            if child.is_file():
                child.unlink()
            else:
                self.rm_tree(child)
        pth.rmdir()

    # USEFUL FUNCTIONS

    # NAMESPACES
    def print_namespaces(self):
        self.print_folders(self.root_folder)

    def select_namespace(self, path):
        if not isinstance(path, Path):
            path = Path().home() / ".ata" / path

        if path.exists() and path.is_dir():
            self.current_namespace = path
        else:
            print(f"Error: {path} does not exist or is not a folder.")

    def create_namespace(self, path):
        if not isinstance(path, Path):
            path = Path().home() / ".ata" / path

        if path.exists() and path.is_dir():
            print(f"Error: {path} already exist!")
        else:
            self.create_folder(path)
            self.current_namespace = path

    def get_current_namespace(self):
        if self.current_namespace:
            return str(self.current_namespace).replace(str(Path().home() / ".ata"), "")[
                1:
            ]
        return None

    def print_task_detail(self, task):
        if not isinstance(task, Path):
            task = self.current_namespace / task

        status_path = Path(task / "status.txt")
        if status_path.exists() and status_path.is_file():
            self.print_file(status_path)

        description_path = Path(task / "description.md")
        if description_path.exists() and description_path.is_file():
            self.print_file(description_path)

        tags_path = Path(task / "tags.txt")
        if tags_path.exists() and tags_path.is_file():
            self.print_file(tags_path)

    def set_task_as(self, task, status):
        status_path = Path(self.current_namespace / task / "status.txt")
        self.overwrite_file(status_path, status)

    def create_task(self):
        name = input("Task name?\n$ ")
        description = input("Task description?\n$ ")
        tags = input("Task tags?\n$ ")
        status = input("Task status (todo, doing, done)?\n$ ")
        while status not in ["todo", "doing", "done"]:
            status = input(
                "Bad task status, please try again:\nTask status (todo, doing, done)?\n$ "
            )

        if self.current_namespace:
            if not Path(self.current_namespace / name).exists():
                self.create_folder(self.current_namespace / name)
                self.create_file(Path(self.current_namespace / name) / "description.md")
                self.overwrite_file(
                    Path(self.current_namespace / name) / "description.md", description
                )
                self.create_file(Path(self.current_namespace / name) / "status.txt")
                self.overwrite_file(
                    Path(self.current_namespace / name) / "status.txt", status
                )
                self.create_file(Path(self.current_namespace / name) / "tags.txt")
                self.overwrite_file(
                    Path(self.current_namespace / name) / "tags.txt", tags
                )
            else:
                print(
                    f"Error: This task ({Path(self.current_namespace / name)}) already exists."
                )
        else:
            print("Error: You must select a namespace first.")

    def create_subtask(self, task_name):
        name = input("Subtask name?\n$ ")
        description = input("Subtask description?\n$ ")
        tags = input("Subtask tags?\n$ ")
        status = input("Subtask status (todo, doing, done)?\n$ ")
        while status not in ["todo", "doing", "done"]:
            status = input(
                "Bad Subtask status, please try again:\nSubtask status (todo, doing, done)?\n$ "
            )

        if self.current_namespace:
            if not Path(self.current_namespace / task_name).exists():
                print("Error: The task does not exist.")
            else:
                if not Path(self.current_namespace / task_name / name).exists():
                    self.create_folder(self.current_namespace / task_name / name)
                    self.create_file(
                        Path(self.current_namespace / task_name / name)
                        / "description.md"
                    )
                    self.overwrite_file(
                        Path(self.current_namespace / task_name / name)
                        / "description.md",
                        description,
                    )
                    self.create_file(
                        Path(self.current_namespace / task_name / name) / "status.txt"
                    )
                    self.overwrite_file(
                        Path(self.current_namespace / task_name / name) / "status.txt",
                        status,
                    )
                    self.create_file(
                        Path(self.current_namespace / task_name / name) / "tags.txt"
                    )
                    self.overwrite_file(
                        Path(self.current_namespace / task_name / name) / "tags.txt",
                        tags,
                    )
                else:
                    print(
                        f"Error: This subtask ({Path(self.current_namespace / task_name / name)}) already exists."
                    )
        else:
            print("Error: You must select a namespace first.")

    def edit_description(self, name):
        description = input("Task description?\n$ ")
        if self.current_namespace:
            if Path(self.current_namespace / name).exists():
                self.create_file(Path(self.current_namespace / name) / "description.md")
                self.overwrite_file(
                    Path(self.current_namespace / name) / "description.md", description
                )
            else:
                print("Error: The task does not exist.")
        else:
            print("Error: You must select a namespace first.")

    def edit_tags(self, name):
        tags = input("Task tags?\n$ ")
        if self.current_namespace:
            if Path(self.current_namespace / name).exists():
                self.create_file(Path(self.current_namespace / name) / "tags.txt")
                self.overwrite_file(
                    Path(self.current_namespace / name) / "tags.txt", tags
                )
            else:
                print("Error: The task does not exist.")
        else:
            print("Error: You must select a namespace first.")

    def delete_task(self, name):
        if Path(self.current_namespace / name).exists():
            delete_input = input(
                "If you're sure, enter the name of the task one more time:\n$ "
            )
            if delete_input == name:
                print("Fine, deleting task.")
                self.rm_tree(Path(self.current_namespace / name))
            else:
                print("Ok, doing nothing.")
        else:
            print("Error: The task does not exist.")

    def setup_repo(self):
        git = HandleGitRepo()
        git.setup_github_gitlab_other_repo(self)
