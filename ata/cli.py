from ata.files import HandleFilesAndFolders
from ata.utils import get_version
from re import search


def cli(*argv, **kwargs):
    if len(argv):
        todo = HandleFilesAndFolders(argv[0])
    else:
        todo = HandleFilesAndFolders()
    user_input = ""

    print(f"Welcome to ata's Version {get_version()} CLI!\t\t(use h for help)")

    while user_input not in ["q", "quit", "exit", "e", "esc :q"]:
        namespace = ""
        if todo.get_current_namespace() is not None:
            namespace = f"({todo.get_current_namespace()}) "
        user_input = input(f"\n{namespace}What to do?\n$ ")

        # Display this help.
        if user_input in ["h", "help"]:
            print(
                (
                    "\nHelp\n"
                    "-- X are numbers, NAME are str (that can't start by a number)--\n\n"
                    " h           Display this help.\n"
                    " n           List namespaces.\n"
                    " nX          Select namespace X.\n"
                    " n NAME      Create a namespace named NAME.\n"
                    " t           List tasks of namespace.\n"
                    " tt          List tasks and subtasks of namespace.\n"
                    " t NAME      Create new task NAME in current namespace.\n"
                    " X NAME      Create new task NAME (subtask of task X).\n"
                    " X           List details of task X.\n"
                    " X Y         List details of task Y (subtask of X).\n"
                    " Xd          Mark task X as done.\n"
                    " Xt          Mark task X as todo.\n"
                    " Xdo         Mark task X as doing.\n"
                    " Xed         Edit description of task X.\n"
                    " Xet         Edit tags of task X.\n"
                    " delete X    Remove task in current namespace.\n"
                    " q           Quit this program.\n"
                )
            )
        # List namespaces
        elif user_input == "n":
            print("Namespaces:")
            ...
        # Select namespace X.
        elif search('^n[1-9][0-9]*$', user_input):  # may not start by a zero
            print(f"Select namespace {user_input[1:]}")
            ...
        # Create a namespace named NAME.
        elif search('^n .*$', user_input):
            print(f"Create namespace {user_input[2:]}")
            ...
        # List tasks of namespace.
        elif user_input == "t":
            print("List tasks of namespace")
            ...
        # List tasks and subtasks of namespace.
        elif user_input == "tt":
            print("List tasks & subtasks of namespace")
            ...
        # Create new task NAME in current namespace.
        elif search('^t .*$', user_input):
            print(f"Create task {user_input[2:]}")
            ...
        # Create new task NAME (subtask of task X).
        # TODO
        # List details of task X.
        elif search('^[1-9][0-9 ]*$', user_input):
            print(f"List details of task {user_input}")
            ...
        # List details of task Y (subtask of X).
        # TODO
        # Mark task X as done.
        elif search('^[1-9][0-9 ]d*$', user_input):
            print("Set task status to done.")
        # Mark task X as todo.
        elif search('^[1-9][0-9 ]t*$', user_input):
            print("Set task status to todo.")
        # Mark task X as doing.
        elif search('^[1-9][0-9 ]do*$', user_input):
            print("Set task status to doing.")
        # Edit description of task X.
        elif search('^[1-9][0-9 ]ed*$', user_input):
            print("Edit task description.")
        # Edit tags of task X.
        elif search('^[1-9][0-9 ]et*$', user_input):
            print("Edit task description.")
        # Remove task in current namespace.
        elif search('^delete [1-9][0-9 ]*$', user_input):
            print("Remove task.")
        elif user_input in ["q", "quit", "exit", "e", "esc :q"]:
            print("Exit!")
        else:
            print(f"{user_input} means nothing. Use h for help.")
