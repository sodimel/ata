from subprocess import run


class HandleGitRepo:
    def setup_github_gitlab_other_repo(self, todo):
        print("Setup a repo for your current namespace:")
        print("  1: I already have an empty repo")
        print("  2: I want to create a repo using Gitlab")
        print("  3: I want to create a repo using Github (boo microsoft :c)")
        print("  4: I don't know what you're talking about")

        user_input = ""

        while user_input not in ["1", "2", "3", "4"]:
            user_input = input("$ ")
            if user_input == "1":
                print(
                    "And that's awesome! Copy the SSH repo link, and paste it here please:"
                )
                repo_url = input("$ ")
            elif user_input == "2":
                print(
                    "Setting up a repo on Gitlab is fairly easy ; you'll need git, maybe a ssh key, and a list of things to do:"
                )
                print(
                    "  1) Here's Gitlab signup direct URL: https://gitlab.example.com/users/sign_up\n     Create an account here if you don't have one."
                )
                print(
                    "  2) Generate a ssh key (if you don't have one) by following the tutorial here: https://docs.gitlab.com/ee/user/ssh.html#generate-an-ssh-key-pair\n     Here's the part that tells you how to add your key to your Gitlab account: https://docs.gitlab.com/ee/user/ssh.html#add-an-ssh-key-to-your-gitlab-account\n     And here's the direct URL to your own SSH Keys config page for your gitlab account: https://gitlab.com/-/profile/keys"
                )
                print(
                    "  3) You then need to create a repo on your account (set it to private), and copy its url.\n     Gitlab create repo direct URL: https://gitlab.com/projects/new#blank_project"
                )
                print(
                    (
                        "\nCreate the project using these info:\n"
                        f"  Name: '{todo.get_current_namespace()} namespace from ATA'.\n"
                        "  Project url: pick your username.\n"
                        f"  Project slug: '{todo.get_current_namespace()}' but slugified.\n"
                        "  Project description: You can put something like 'my todo archive from https://gitlab.com/sodimel/ata app', or something like this.\n"
                        "  Visibility level: private.\n"
                        '  UNTICK "Initialize repository with a README"\n'
                        "  That's all folks!\n\n"
                        "  Now click on the green 'clone' button.\n"
                        "  Copy the 'Clone with ssh' code.\n"
                        "  And paste it here:\n"
                    )
                )
                repo_url = input("$ ")
            elif user_input == "3":
                print(
                    "Setting up a repo on Github is fairly easy ; you'll need git, maybe a ssh key, and a list of things to do:"
                )
                print(
                    "  1) Here's Github signup direct URL: https://github.com/join\n     Create an account here if you don't have one."
                )
                print(
                    "  2) Generate a ssh key (if you don't have one) by following the tutorial here: https://docs.github.com/en/authentication/connecting-to-github-with-ssh/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent\n     Here's the part that tells you how to add your key to your Github account: https://docs.github.com/en/authentication/connecting-to-github-with-ssh/adding-a-new-ssh-key-to-your-github-account\n     And here's the direct URL to your own SSH Keys config page for your Github account: https://github.com/settings/keys"
                )
                print(
                    "  3) You then need to create a repo on your account (set it to private), and copy its url.\n     Github create repo direct URL: https://github.com/new"
                )
                print(
                    (
                        "\nCreate the project using these info:\n"
                        "  Owner: pick your username.\n"
                        f"  Repository name: '{todo.get_current_namespace()}' but slugified.\n"
                        "  Project description: You can put something like 'my todo archive from https://gitlab.com/sodimel/ata app', or something like this.\n"
                        "  Tick the 'private' radio button.\n"
                        '  DO NOT TICK "Initialize this repository with:  Add a README file"\n'
                        '  Keep the "Add .gitignore" to None.\n'
                        "  Put whatever license you want (you can keep None).\n"
                        "  That's all folks!\n\n"
                        "  Now click on the green 'code' button.\n"
                        "  Select 'SSH', and copy the link of the repo.\n"
                        "  And paste it here:\n"
                    )
                )
                repo_url = input("$ ")
            elif user_input == "4":
                print(
                    "Here's some doc:\n  https://en.wikipedia.org/wiki/Git\n    -> Git is the thing that synchronize your namespace data.\n  https://en.wikipedia.org/wiki/Forge_(software)\n    -> A Forge is a software that may use git that do a lots of things.\n       One of those things is interesting for us: Give a free access to a private git repo somewhere\n       on the Internet that allows this program to save the state of your namespace."
                )
                print(
                    "\nNow you may want to register on a Forge website, like Gitlab or Github (or another site if you prefer it)."
                )
                user_input == ""
            else:
                print(f"{user_input} does not means anything. Please retry.")

        if user_input == "":
            print("Hmm, there's an error with your repo link, please retry.")
            exit()
        else:
            self.setup_git_repo(todo, repo_url)

    def setup_git_repo(self, todo, repo_url):
        print("Creating empty repo...")
        proc = run(
            f"git init {todo.current_namespace} --initial-branch=main",
            shell=True,
            capture_output=True,
        )
        if proc.returncode != 0:
            proc = run(
                f"git init {todo.current_namespace}", shell=True, capture_output=True
            )
            if proc.returncode != 0:
                print("Error:")
                print(proc.stdout.decode())
                print(proc.stderr.decode())
                exit()
            proc = run(
                f"git -C {todo.current_namespace} symbolic-ref HEAD refs/heads/main",
                shell=True,
                capture_output=True,
            )
            if proc.returncode != 0:
                print("Error:")
                print(proc.stdout.decode())
                print(proc.stderr.decode())
                exit()
        print(f"Adding {repo_url} as remote origin...")
        proc = run(
            f"git -C {todo.current_namespace} remote add origin {repo_url}",
            shell=True,
            capture_output=True,
        )
        if proc.returncode != 0:
            print("Error:")
            print(proc.stdout.decode())
            print(proc.stderr.decode())
            exit()
        print("Staging all content...")
        proc = run(
            f"git -C {todo.current_namespace} add .", shell=True, capture_output=True
        )
        if proc.returncode != 0:
            print("Error:")
            print(proc.stdout.decode())
            print(proc.stderr.decode())
            exit()
        print("Committing...")
        proc = run(
            f"git -C {todo.current_namespace} commit -m 'First commit from ata!!!1!'",
            shell=True,
            capture_output=True,
        )
        if proc.returncode != 0:
            print("Error:")
            print(proc.stdout.decode())
            print(proc.stderr.decode())
            exit()
        print("Pushing...")
        proc = run(
            f"git -C {todo.current_namespace} push -u origin main",
            shell=True,
            capture_output=True,
        )
        if proc.returncode != 0:
            print("Error:")
            print(proc.stdout.decode())
            print(proc.stderr.decode())
            exit()
        print("Done! Your todos are now stored somewhere else!")
