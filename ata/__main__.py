import argparse

from ata.app import app
from ata.cli import cli

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--cli",
        dest="cli",
        help="Run the CLI instead of the TUI.",
        nargs="?",
        const=True,
        default=False,
    )
    args = parser.parse_args()

    if args.cli:
        if isinstance(args.cli, bool):
            cli()
        else:
            cli(args.cli)
    else:
        app()
