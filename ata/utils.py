from ata.__version__ import version


def get_version():
    return version
