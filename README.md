<div align="center"><img src="logo.png" alt="ata logo" /></div>

# Another Todo App, Version 0.0.1

## Current status


<div align="center">
<a href="https://gifcities.org/?q=under+construction"><img src="https://gitlab.com/sodimel/ata/uploads/94b7a5cad6db2c788753350cd0951dfd/under_construction.gif" /></a>

**Very early WIP, not really useful or really working.**

**No tests. No fancy info. Do not use it.**

<a href="https://gifcities.org/?q=under+construction"><img src="https://gitlab.com/sodimel/ata/uploads/94b7a5cad6db2c788753350cd0951dfd/under_construction.gif" /></a>
</div>

## Install

```bash
python3 -m pip install ata  # not on pypi now, but maybe in a near future
```

## Launch

### TUI

```bash
python3 -m ata
# You can define an alias for this, like this:
# alias ata="python3 -m ata"
```

### CLI

```bash
python3 -m ata --cli [namespace]
# You can specify a namespace name in the command,
# or select the namespace from the cli.
```

## Infos

* [x] ata can handle namespaces (projects), tasks and (((...)sub)sub)subtasks.
* [x] Tasks can have tags, description, and status (todo, doing, done).
* [ ] You can add new status, tags, and templates for descriptions.
* [ ] You can even add a new "task" template (containing a list of subtasks (including tags and descriptions)).
* [ ] You can access ata's TUI by launching it with no args.
* [x] You can access the cli using `python3 -m ata --cli`. You will then be prompted what to do (display a task or set of tasks, add new task, move task to done or todo, add subtask to task...).
* [ ] You can synchronize your namespaces using a git repo (may be useful when using multiple computers). The synchronization is done per namespace.

## More infos

All the data is stored in plain text in a small `.ata` folder in your home folder.

A namespace is a folder, a task is another folder inside it. A subtask is another folder inside it, and a sub-subtask is another folder [...].

Description of a task is a file in the task folder, which is named `description.md`.

Tags are comma-separated ascii strings in a file named `tags.txt`.

Status of a task is stored in a `status.txt` file. Custom status can be defined here. Recognized status are "todo", "done", or "doing" (they will have special icons next to them).

### Example of a task with subtasks:

```bash
.ata/
    cool project/  # namespace (can have spaces)
        .git/  # (synchronized with a git repo somewhere)
        first task/  # task
            status.txt
                | done  # content of file
            description.md
                | Creating my *first* task!
            tags.txt
                | important, another tag
            subtask/  # subtask
                status.txt
                    | done
                description.md
                    |
                tags.txt
                    |
        second task/  # task
            status.txt
                | todo
            description.md
                | My second tasks. Its ***urgent***!
            tags.txt
                | important
```

## Develop

```bash
git clone https://gitlab.com/sodimel/ata.git
cd ata
python3 -m venv .venv
. .venv/bin/activate
python3 -m pip install -e ata
# update your code, and test it
# submit PR
```

## Screenshots

![v0.0.1](https://gitlab.com/sodimel/ata/uploads/4bb1d84856acd06cb5ebfcaa3b947e00/image.png)

> v0.0.1 − help of CLI

## TODO − CLI

* rewrite instructions in order to make them faster to write:
  * [ ] `h`: Display help
  * [ ] `n`: List namespaces
  * [ ] `nX`: Select namespace X (n1 for first, n2 second, ...)
  * [ ] `n NAME`: Create new namespace named NAME
  * [ ] `s`: Synchronize current namespace. If git repo is found: pull, commit, push. Else: open setup repo, commit, push.
  * [ ] `t`: List tasks of current namespace.
  * [ ] `tt`: List all tasks and ((...)sub)subtasks of current namespace.
  * [ ] `X`: List task X (t1 for first task...): details of the task, list of subtasks.
  * [ ] `X Y`: List subtask Y (of task X): details of the subtask, list of sub-subtasks.
    * [ ] And so `1 11 2 4` is listing sub-sub-subtask 4 (from sub-subtask 2 (from subtask 12 (from task 1))).
  * [ ] `Xd`: Set task X of current namespace to "done".
  * [ ] `Xt`: Set task X of current namespace to "todo".
  * [ ] `Xdo`: Set task X of current namespace to "doing".
  * [ ] `t NAME`: Create new task in current namespace named NAME.
  * [ ] `X NAME`: Create new subtask of task X in current namespace named NAME.
    * [ ] And so `3 1 2 'my task'` create a subtask named 'my task' to the 2nd task, who's a subtask of the 1 task who's a subtask of the third task.
  * [ ] `Xed`: Edit description of task X.
  * [ ] `Xet`: Edit tags of task X.
  * [ ] `delete X`: Delete task X.

## More?

Coming −maybe− soon :)
